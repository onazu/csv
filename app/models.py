import datetime
import decimal

from sqlalchemy import Integer, String, DateTime, Boolean, Numeric
from sqlalchemy.orm import mapped_column, Mapped
from app.db_conf import Base
from app.history_meta import Versioned


class TagDBModel(Versioned, Base):
    __tablename__ = 'tag_table'

    id: Mapped[int] = mapped_column(primary_key=True)
    tag_name: Mapped[str] = mapped_column(String(50))
    available: Mapped[bool] = mapped_column(Boolean)
    currency: Mapped[str] = mapped_column(String(50))
    address_pub: Mapped[str] = mapped_column(String(200), unique=True)
    seed: Mapped[str] = mapped_column(String(200), unique=True)
    balance: Mapped[decimal.Decimal] = mapped_column(Numeric)
    value_acquisition: Mapped[int] = mapped_column(Integer)
    date_acquisition: Mapped[datetime.datetime] = mapped_column(DateTime(timezone=True))