from graphene_sqlalchemy import SQLAlchemyObjectType
from app.models import TagDBModel

class TagObject(SQLAlchemyObjectType):
    class Meta:
        model = TagDBModel

class TagObjectHistory(SQLAlchemyObjectType):
    class Meta:
        model = TagDBModel.__history_mapper__.class_