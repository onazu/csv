import pandas as pd
from graphene import ObjectType, Mutation, Boolean, String, Int, Decimal
from app.models import TagDBModel
from app.db_conf import session
import logging

TagDBModelHistory = TagDBModel.__history_mapper__.class_

class ImportSpreadsheet(Mutation):
    class Arguments:
        spreadsheet = String(required=True)

    success = Boolean()

    def mutate(self, info, spreadsheet):
        try:

            df = pd.read_excel(spreadsheet)
            data = []

            for index, row in df.iterrows():
                available = row.get("available")
                tag_name = row.get("tag_name")
                currency = row.get("currency")
                address_pub = row.get("address_pub")
                seed = row.get("seed")
                balance = row.get("balance")
                value_acquisition = row.get("value_acquisition")
                date_acquisition = row.get("date_acquisition")

                data.append((
                    available,
                    tag_name,
                    currency,
                    address_pub,
                    seed,
                    balance,
                    value_acquisition,
                    date_acquisition
                ))

                print(f"A: {available}, C: {currency}, B: {balance}, Tag Name: {tag_name}, Address Pub: {address_pub}, Seed: {seed}, Value Acquisition: {value_acquisition}, Date Acquisition: {date_acquisition}")

            for spreadsheet_data in data:
                new_tag = TagDBModel(
                    available=spreadsheet_data[0],
                    tag_name=spreadsheet_data[1],
                    currency=spreadsheet_data[2],
                    address_pub=spreadsheet_data[3],
                    seed=spreadsheet_data[4],
                    balance=spreadsheet_data[5],
                    value_acquisition=spreadsheet_data[6],
                    date_acquisition=spreadsheet_data[7]
                )
                session.add(new_tag)

            session.commit()

            return ImportSpreadsheet(success=True)
        except Exception as e:
            logging.error(f"Erreur lors de la mutation : {str(e)}")
            return ImportSpreadsheet(success=False)


class UpdateTag(Mutation):
    class Arguments:
        tag_id = Int(required=True)
        available = Boolean()
        tag_name = String()
        currency = String()
        address_pub = String()
        seed = String()
        balance = Decimal()
        value_acquisition = Int()
        date_acquisition = String()

    success = Boolean()
    message = String()

    def mutate(self, info, tag_id, available=None, tag_name=None, currency= None, address_pub=None, seed=None, balance=None, value_acquisition=None, date_acquisition=None):
        try:
            tag = session.query(TagDBModel).filter_by(id=tag_id).first()

            if available is not None:
                tag.available = available
            if tag_name is not None:
                tag.tag_name = tag_name
            if currency is not None:
                tag.currency = currency
            if address_pub is not None:
                tag.address_pub = address_pub
            if seed is not None:
                tag.seed = seed
            if balance is not None:
                tag.balance = balance
            if value_acquisition is not None:
                tag.value_acquisition = value_acquisition
            if date_acquisition is not None:
                tag.date_acquisition = date_acquisition

            session.commit()
            return UpdateTag(success=True, message="Tag updated successfully")
        except Exception as e:
            return UpdateTag(success=False, message=str(e))

class RestoreTagVersion(Mutation):
    class Arguments:
        tag_version_id = Int(required=True)

    success = Boolean()

    def mutate(self, info, tag_version_id):
        history_version = session.query(TagDBModelHistory).filter(TagDBModelHistory.version == tag_version_id).all()
        if history_version:
            for tag in history_version:
                existing_tag = session.query(TagDBModel).filter_by(id=tag.id).first()

                if existing_tag:
                    existing_tag.tag_name = tag.tag_name
                    existing_tag.available = tag.available
                    existing_tag.currency = tag.currency
                    existing_tag.address_pub = tag.address_pub
                    existing_tag.seed = tag.seed
                    existing_tag.balance = tag.balance
                    existing_tag.value_acquisition = tag.value_acquisition
                    existing_tag.date_acquisition = tag.date_acquisition

            session.commit()

            return RestoreTagVersion(success=True)
        else:
            return RestoreTagVersion(success=False)



class Mutation(ObjectType):
    import_Spreadsheet = ImportSpreadsheet.Field()
    update_tag = UpdateTag.Field()
    restore_tag_version = RestoreTagVersion.Field()
