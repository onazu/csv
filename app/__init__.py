from graphene import Schema
from flask import Flask
from flask_graphql import GraphQLView
from flask_cors import CORS
from app.db_conf import Base, engine, session
from app.models import TagDBModel
from app.schemas import TagObject
from app.mutation import Mutation
from app.query import Query
import logging

app = Flask(__name__)
CORS(app)

# Configurez le logger
logging.basicConfig(level=logging.DEBUG)

# Création de la table dans la base de données (si elle n'existe pas déjà)
Base.metadata.create_all(engine)

schema = Schema(query=Query, mutation=Mutation)

app.add_url_rule('/graphql', view_func=GraphQLView.as_view(
    'graphql',
    schema=schema,
    graphiql=True
))
