from sqlalchemy import create_engine
from app.history_meta import versioned_session
from sqlalchemy.orm import Session, DeclarativeBase

# Configuration de la base de données SQLAlchemy
DATABASE_URI = 'postgresql://XXXXXXXXx@cornelius.db.elephantsql.com/ydbsiitk'
engine = create_engine(DATABASE_URI, echo=True)
session = Session(engine)
versioned_session(session)


class Base(DeclarativeBase):
    pass
