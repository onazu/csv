from graphene import ObjectType, Field, List, Int
from sqlalchemy import desc

from app.models import TagDBModel
from app.schemas import TagObject, TagObjectHistory
from app.db_conf import session

TagDBModelHistory = TagDBModel.__history_mapper__.class_

class Query(ObjectType):
    all_tags = List(TagObject)
    tagById = Field(TagObject, tagId=Int())
    tagVersions = List(TagObjectHistory)

    def resolve_all_tags(self, info):
        return session.query(TagDBModel).all()

    def resolve_tagById(self, info, tagId):
        return session.query(TagDBModel).filter_by(id=tagId).first()

    def resolve_tagVersions(self, info):
        versions_by_changed = session.query(TagDBModelHistory).order_by(desc(TagDBModelHistory.changed)).all()
        return versions_by_changed



