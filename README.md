# Requetes GraphIQL

Exemple de query suivant le tag
```
{
  tagById(tagId: 1) {
    tagName
    currency
  }
}
```
Exemple de mutation import csv
```
mutation {
  importSpreadsheet(
    spreadsheet: "D:/Projet/graphql/DataTest/data.xlsx"
  ) {
    success
  }
}
```
Exemple de update nimporte quel champs de la table tag
```
mutation {
  updateTags(tagUpdates: [
    { tagId: 1, tagName: "NouveauNom1", currency: "NouvelleDevise1" },
    { tagId: 2, tagName: "NouveauNom2" }
  ]) {
    success
    messages
  }
}
```
Exemple de query les derniers changements dans l'ordre
```
{
  tagVersions {
    id
    version
    changed
  }
}
```
Exemple de mutate met tous les tags a une version definit => pas bon a revoir 
```
mutation {
  restoreTagVersion(tagVersionId: 1) {
    success
  }
}
```

## TODO
- Restorer la bdd à un etat definit (temporel) 
- 
